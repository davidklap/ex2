#include "Cell.h"
#include "Nucleus.h"
#include "Ribosome.h" 
#include "Protein.h"
#include "Mitochondrion.h" 
#include "AminoAcid.h" 
#include <iostream>
void  Cell :: init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	_nucleus.init(dna_sequence);
	_mitochondrion.init();
	_glocus_receptor_gene = glucose_receptor_gene;
	//pro.init();
	//ribo.create_protein(nuc.get_RNA_transcript);
} 

bool Cell::get_ATP()
{
	std::string str= _nucleus.get_RNA_transcript(_glocus_receptor_gene);
	Protein *protein = _ribosome.create_protein(str);
	if (!protein)
	{
		std::cerr << "cannot creit a protein";
		_exit(1);
	}
	else
	{
		
		_mitochondrion.insert_glucose_receptor(*protein);
		_mitochondrion.produceATP();
		if (_mitochondrion.get_has__glocuse_receptor())
		{
			_units_atp = 100;
			return true;
		}
		else {
			return false;
		}
	
	}
	


}