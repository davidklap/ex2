#pragma once
#define NUM_OF_AMINO_ACID 7
#define MIN_GLUCODE_LEVEL 50 
#include"Protein.h"
class Mitochondrion
{
public:
	void init();
	void insert_glucose_receptor( Protein& protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;
	bool get_has__glocuse_receptor();

private:
	unsigned int  _glocuse_level;
	bool _has__glocuse_receptor;
	void set_has__glocuse_receptor(bool glocuse_receptor);
	bool get_glucose_receptor()const;
	int get_glucose_level()const;
};

