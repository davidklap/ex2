#include "Mitochondrion.h"
#include "AminoAcid.h"
#include "Nucleus.h"
#include"Protein.h"
#include "Ribosome.h"

void Mitochondrion::init()
{
	_glocuse_level = 0;
	_has__glocuse_receptor = false;
}
void Mitochondrion::set_has__glocuse_receptor(bool glocuse_receptor)
{
	_has__glocuse_receptor = glocuse_receptor;
}

bool Mitochondrion::get_has__glocuse_receptor()
{
	return _has__glocuse_receptor;
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	_glocuse_level = glocuse_units;
}

bool Mitochondrion:: get_glucose_receptor()const
{
	return _has__glocuse_receptor;
}

int Mitochondrion::get_glucose_level()const
{
	return _glocuse_level;
}
bool  Mitochondrion::produceATP() const
{
		return get_glucose_receptor() && get_glucose_level() > MIN_GLUCODE_LEVEL;
}

void Mitochondrion::insert_glucose_receptor( Protein& protein)
{
	AminoAcid amino_arr[] = { ALANINE,LEUCINE,GLYCINE,HISTIDINE ,LEUCINE, PHENYLALANINE,AMINO_CHAIN_END };// an arr of the amono acid i nthe correct order that thay spoose to be 
	int i = 0;
	AminoAcidNode *node = protein.get_first();
	unsigned int  count = 0;
	// check if all the same and buy the same order 
	while (amino_arr[i] == node->get_data()&&count<NUM_OF_AMINO_ACID)
	{
		count++;
	}
	if (count == NUM_OF_AMINO_ACID)
	{
		set_has__glocuse_receptor(true);
	}
}
