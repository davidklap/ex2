#ifndef NUCLEUS_H
#define NUCLEUS_H
#include <iostream>
#include <string>
//////////////////////////// Gene class /////////////////////////////////////////
class Gene
{
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	// geters
	int get_start()const;
	int get_end()const;
	bool is_on_complementary_dna_strand() const;
	// stetrs 
	void set_start(unsigned int start);
	void set_end(unsigned int end);
	void setOn_complementary_dna_strand( bool on_complementary_dna_strand);

private:
	unsigned int _start; 
	unsigned int _end;
	bool _on_complementarys_dna_trand;
};

///////////////////////////////////////////Nucleus class ///////////////////////////////////////

class Nucleus
{
public:

	void setDna_starnd(std::string dna);
	void setcomplementary_DNA_strand( std::string complemtry);
	std::string  get_Dna_strand()const;
	std::string get_complementary_DNA_strand()const;

	/*foncton crete the compkence dna for the dna that enter and save it */
	void init(const std::string dna_sequence);

	/*fonncton fet the data from the gene(if its include the start and the end )
	input : the genr objrct /
	output: the arna */
	std::string get_RNA_transcript(const Gene& gene) const;

	/*finction returns the swap of the dna */
	std::string get_reversed_DNA_strand() const;
	
	/*check hoe much time the codons is in the dna */
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
private:
	std::string  _DNA_strand;
	std::string _complementary_DNA_strand;
};

#endif //! NUCLEUS_H

