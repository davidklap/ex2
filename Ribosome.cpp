#include "Ribosome.h"
#include "AminoAcid.h"
#include "Protein.h"
#include "Nucleus.h"
Protein* Ribosome:: create_protein(std::string& RNA_transcript) const
{
	Protein* protein = new Protein;
	protein->init();
	unsigned int rna_aize = RNA_transcript.length();
	std::string first_noc;
	 AminoAcid amino_acid;
	while (RNA_transcript.length() >= MIN_NOC_ZIZE)
	{
		first_noc = RNA_transcript.substr(0, MIN_NOC_ZIZE);
		amino_acid=  get_amino_acid(first_noc);
		// chrck that the fonctuon fondt return unknow
		if (amino_acid == UNKNOWN)
		{
			protein->clear();
			return NULL;
		}
		else
		{
			protein->add(amino_acid);
		}
		RNA_transcript = RNA_transcript.substr(MIN_NOC_ZIZE, RNA_transcript.length() - MIN_NOC_ZIZE);
	}
	return protein;
}