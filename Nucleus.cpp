#include "Nucleus.h"
#include <iostream>
#include <string>
////////////////////////////////// Gene class impolense/////////////////////////////////////////////////
void Gene:: init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	set_start(start);
	set_end(end);
	 setOn_complementary_dna_strand( on_complementary_dna_strand);
}

int Gene::get_start() const
{
	return _start;
}

int Gene::get_end() const
{
	return _end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return _on_complementarys_dna_trand;
}

void Gene::set_start(unsigned int start)
{
	_start = start;
}

void Gene::set_end(unsigned int end)
{
	_end = end;
}

void Gene::setOn_complementary_dna_strand( bool on_complementary_dna_strand)
{
	_on_complementarys_dna_trand = on_complementary_dna_strand;
}

/////////////////////////////////////////////////////// Nucleus class ipmolense //////////////////////////////

// geters
std::string Nucleus::get_Dna_strand() const
{
	return  _DNA_strand;
}

std::string Nucleus::get_complementary_DNA_strand() const
{
	return  _complementary_DNA_strand;
}
 // seters
void Nucleus::setDna_starnd(std::string dna)
{
	_DNA_strand = dna;
}

void Nucleus::setcomplementary_DNA_strand(std::string complemtry)
{
	_complementary_DNA_strand = complemtry;
}
void Nucleus::init(const std::string dna_sequence)
{
	int i = 0;
	int size = dna_sequence.length(); // becuse we dont want to do it twice
	 std::string complementary_dna_strand = "";
	 for (i = 0; i < size; i++)
	 {
		 switch (dna_sequence[i])
		 {
		 case 'G':
			 complementary_dna_strand.append("C");
			 break;
		 case 'C':
			 complementary_dna_strand.append("G");
			 break;
		 case 'A':
			 complementary_dna_strand.append("T");
			 break;
		 case 'T':
			 complementary_dna_strand.append("A");
			 break;
		 default:
			std::cerr << "anligel dna sequnce!"; // its mean that its a i ligel dna seqqnce
			_exit(1);
			break;
		 }
	 }
	 setDna_starnd(dna_sequence);
	 setcomplementary_DNA_strand(complementary_dna_strand);
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string rna = "";
	std::string dna_str = "";
	int i = 0;
	if (gene.is_on_complementary_dna_strand())
	{
		dna_str = get_complementary_DNA_strand();
	}
	else
	{
		dna_str = get_Dna_strand();
	}
	// crete the rna fronm the dna
	if (!dna_str.length())
	{
		return "";
	}
	rna = dna_str.substr(gene.get_start(), gene.get_end() - gene.get_start()-1);
	// sercj the t of found seitch it to u 
	for (; i < rna.length(); i++)
	{
		if (rna[i] == 'T')
			rna[i] = 'U';
	}
	return rna;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string str = get_Dna_strand();
	reverse(str.begin(), str.end());
	return str;
}

unsigned int  Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{

	int M = codon.length();
	int N = get_Dna_strand().length();
	int res = 0;

	/* A loop to slide  one by one */
	for (int i = 0; i <= N - M; i++)
	{
		/* For current index i, check for
		   pattern match */
		int j;
		for (j = 0; j < M; j++)
			if (get_Dna_strand()[i + j] != codon[j])
				break;
		if (j == M)
		{
			res++;
			j = 0;
		}
	}
	return res;
}



