#ifndef CELL_H
#define CELL_H 
#include "Nucleus.h"
#include "AminoAcid.h"
#include "Ribosome.h"
#include "Mitochondrion.h"
#include <iostream>
class Cell
{
public:
	bool get_ATP();
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	
private:
	Nucleus _nucleus;
	Ribosome  _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _units_atp;
};
#endif // CELL_H